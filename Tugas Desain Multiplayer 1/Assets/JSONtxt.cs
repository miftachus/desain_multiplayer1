﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Newtonsoft.Json;

public class JSONtxt : MonoBehaviour
{
    public string InputLinkJSON;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI emailText;

    private void Awake()
    {
        WWW req = new WWW(InputLinkJSON);
        StartCoroutine(ProcessRequestJSON(req));
    }

    IEnumerator ProcessRequestJSON(WWW jsonReq)
    {
        yield return jsonReq;
        nameText.text = GetCharacterName(jsonReq.text);
        emailText.text = GetCharacterEmail(jsonReq.text);
    }

    private string GetCharacterName(string json)
    {
        List<Character> CharacterInfo = JsonConvert.DeserializeObject<List<Character>>(json);

        return CharacterInfo[14].name.ToString();
    }

    private string GetCharacterEmail(string json)
    {
        List<Character> CharacterInfo = JsonConvert.DeserializeObject<List<Character>>(json);

        return CharacterInfo[14].email.ToString();
    }
}

public class Character
{
    public string name;
    public string email;

    public Character(string name, string email)
    {
        this.name = name;
        this.email = email;
    }
}